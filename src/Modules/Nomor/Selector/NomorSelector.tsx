import { createSelector } from 'reselect';

const patientState = (state: any) => state.PatientState;
// const patientFormState = (state: any) => state.form.patientForm.values;

export const listSelector = () =>
  createSelector(patientState, state => state.list);
// export const idSelector = () =>
//   createSelector(patientState, state => state.selectedIdPatient);
// export const selectedPatientSelector = () =>
//   createSelector(patientState, state => state.selectedPatient);
// export const modalActionSelector = () =>
//   createSelector(patientState, state => state.modalAction);

// //SELECTOR FORM
// export const formNameSelector = () =>
//   createSelector(patientFormState, state => state.name);
// export const formAddressSelector = () =>
//   createSelector(patientFormState, state => state.address);
// export const formDateOfBirthSelector = () =>
//   createSelector(patientFormState, state => state.dateOfBirth);
// export const formGenderSelector = () =>
//   createSelector(patientFormState, state => state.gender);
// export const formNameParentSelector = () =>
//   createSelector(patientFormState, state => state.nameParent);
// export const formTelephoneSelector = () =>
//   createSelector(patientFormState, state => state.telephone);
// export const formNIKSelector = () =>
//   createSelector(patientFormState, state => state.nik);
// export const formNoBPJSSelector = () =>
//   createSelector(patientFormState, state => state.noBPJS);
// export const formRegistrationSelector = () =>
//   createSelector(patientFormState, state => state.registration);
// export const formPoliSelector = () =>
//   createSelector(patientFormState, state => state.poli);