export function fetchPatientListRequested() {
  return {
    type: 'FETCH_PATIENT_LIST_REQUESTED',
  };
}
