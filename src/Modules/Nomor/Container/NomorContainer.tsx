import * as PatientAction from '../Store/NomorAction';
import * as SelectorPatient from '../Selector/NomorSelector';
import * as SelectorTemplate from '../../Template/Selector/TemplateSelector';
import * as TemplateAction from '../../Template/Store/TemplateAction';

import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';

import NomorComponen from '../Componen/NomorComponen';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

function NomorContainer(props) {
  const { actionPatient, listData} = props;

  useEffect(() => {
    actionPatient.fetchPatientListRequested();
  }, [actionPatient]);

  return (
    <NomorComponen data={listData} />
  );
}

const mapStateToProps = createStructuredSelector({
  listData: SelectorPatient.listSelector(),
  modalPatientIsShow: SelectorTemplate.modalPatientSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionPatient: bindActionCreators(PatientAction, dispatch),
  actionTemplate: bindActionCreators(TemplateAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(NomorContainer);