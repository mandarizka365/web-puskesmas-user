import * as OldAction from '../Store/AntrianAction';
import * as SelectorOld from '../Selector/AntrianSelector';
import * as SelectorTemplate from '../../Template/Selector/TemplateSelector';
import * as TemplateAction from '../../Template/Store/TemplateAction';

import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';

import AntrianComponen from '../Componen/AntrianComponen';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

function AntrianContainer(props) {
  const { actionOld, listData } = props;
  
  useEffect(() => {
    actionOld.fetchOldListRequested();
  }, [actionOld]);
  return (
    <AntrianComponen
      data={listData}
      {...props}
    />
  );
}

const mapStateToProps = createStructuredSelector({
  listData: SelectorOld.listSelector(),
  modalOldIsShow: SelectorTemplate.modalOldSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionOld: bindActionCreators(OldAction, dispatch),
  actionTemplate: bindActionCreators(TemplateAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(AntrianContainer);
