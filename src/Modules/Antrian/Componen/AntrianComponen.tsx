import { Card, Layout } from 'antd'

import React from 'react'

const { Content } = Layout;

export default function AntrianComponen(props) {
    const { data } = props;
    return (
        <>
        <Layout>
            <Content className="content-nomor" >
            <Card className="cardNomor">
                <div style={{textAlign:'center'}}>
                    <h2><b>NOMOR ANTRIAN</b></h2><br/>
                    <p style={{textAlign:'center', fontSize:25}}>{data.length}</p>
                </div>
            </Card>
            </Content>
        </Layout>
        </>
    )
}
