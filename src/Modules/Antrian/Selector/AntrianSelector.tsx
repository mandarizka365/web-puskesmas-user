import { createSelector } from 'reselect';

const oldState = (state: any) => state.OldState;

export const listSelector = () =>
  createSelector(oldState, state => state.list);