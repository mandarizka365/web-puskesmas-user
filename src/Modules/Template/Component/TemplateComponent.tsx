import { Col, Layout, Menu, Row } from 'antd';
import React, { Component } from 'react'

import { Link } from 'react-router-dom';
import images from '../../App/images'

const { Header, Content, Footer} = Layout;
export default class TemplateComponent extends Component {

    render() {
        return (
        <Layout>
          <Header className="headertemplate">
            <img src={images.puskesmas} alt="doctor" className="logo"/>
            <Menu className="menuHeader" mode="horizontal">
            <Menu.Item className="menuMenu" key="home" >
              <Link className="linkmenu" to="/">HOME</Link>
            </Menu.Item>
            <Menu.Item className="menuMenu" key="profil" >
              <Link className="linkmenu" to="/profil">PROFIL</Link>
            </Menu.Item>
            <Menu.Item className="menuMenu" key="layanan">
              <Link className="linkmenu" to="/layanan">LAYANAN</Link>
            </Menu.Item>
              <Menu.Item className="menuMenu" key="pendaftaran" >
                <Link style={{color:'cornflowerblue', fontSize: '14px'}} to="/pasienBaru">Pasien Baru</Link>
              </Menu.Item>
              <Menu.Item className="menuMenu" key="pasienLama">
                <Link style={{color:'cornflowerblue', fontSize: '14px'}} to="/pasienLama">Pasien Lama</Link>
              </Menu.Item>
            <Menu.Item className="menuMenu" key="kegiatan">
              <Link className="linkmenu" to="/kegiatan">KEGIATAN</Link>
            </Menu.Item>
            </Menu>
          </Header>
          <Content className="site-layout">
            {this.props.children}
          </Content>
          <Footer className="footer">
          <Row className="rowfooter">
            <Col  xs={24} sm={24} md={24} lg={8} xl={8}>
              <img src={images.puskes} alt="location" height="100"/>
            </Col>
            <Col className="colfooter" xs={24} sm={24} md={24} lg={8} xl={8}>
              <div className="divfooter">
                <h4 className="h4"> KONTAK</h4>
                <ul class="footer-links">
                  <li>
                    <img src={images.location}  alt="location" height="15"/>
                    <span>Jl Letda Sudarmono No. 57 Sempol,<br/>  Kelurahan Kalikajar, Kecamatan <br/>Kalikajar,  Kabupaten Wonosobo,<br/>Jawa Tengah, Indonesia</span>                  </li>
                  <li>
                    <img src={images.call}  alt="call" height="15"/>
                    <span>(0286) 329293</span>
                  </li>
                  <li>
                    <img src={images.email}  alt="email" height="15"/>
                    <span>pusk_kalikajar1@yahoo.co.id</span>
                  </li>
                </ul>
              </div>
            </Col>
            <Col className="colfooter" xs={24} sm={24} md={24} lg={8} xl={8}>
            <iframe title="map" className="iframemap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.4324975209884!2d109.96965431477578!3d-7.417292694648531!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a98b8f37e35ff%3A0xca28abfbe8cae79d!2sPuskesmas%20Kalikajar!5e0!3m2!1sid!2sid!4v1604545114877!5m2!1sid!2sid"
              allowFullScreen aria-hidden="false">
            </iframe>
                
            </Col>
          </Row>
          </Footer>
           
        </Layout>
        );
    }
}