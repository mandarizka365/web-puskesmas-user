//import { Button } from 'antd';

import { Layout } from 'antd';
import ModalLayananContainer from '../Container/ModalLayananContainer';
import React from 'react';
import Table from '../../../Assets/Components/CTable';

const { Content } = Layout;

export default function LayananComponen(props) {
  const { colums, data } = props;
  return (
    <Layout>
      <Content className="content1">
        <div>
            <ModalLayananContainer {...props} />
            <Table columns={colums} data={data} />
        </div>
      </Content>
    </Layout>
  );
}
