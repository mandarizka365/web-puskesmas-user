export default function DoctorValidation(values) {
  const errors: any = {};
  if (!values.name) {
    errors.name = 'Name Required!';
  } else if (values.name.length < 3) {
    errors.name = 'Minimal Name 3 characters';
  }
  // if (!values.address) {
  //   errors.address = 'Address Required!';
  // }
  // if (!values.phone) {
  //   errors.phone = 'Phone Required!';
  // }
  return errors;
}
