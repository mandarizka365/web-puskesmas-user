import * as DoctorAction from '../Store/LayananAction';
import * as SelectorDoctor from '../Selector/LayananSelector';
import * as SelectorTemplate from '../../Template/Selector/TemplateSelector';
import * as TemplateAction from '../../Template/Store/TemplateAction';

import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';

import LayananComponen from '../Component/LayananComponen';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

function LayananContainer(props) {
  const { actionDoctor, listData, actionTemplate } = props;

  const colums = [
    {
      Header: 'Poli',
      accessor: 'poli',
    },
    {
      Header: 'Day',
      accessor: 'day',
    },
    {
      Header: 'Time',
      accessor: 'time',
    },
  ];

  useEffect(() => {
    actionDoctor.fetchDoctorListRequested();
  }, [actionDoctor]);

  const handleAddDoctor = () => {
    actionTemplate.openModal('Doctor');
    actionDoctor.changeModalAction('register');
  };

  const handleCancelModal = () => {
    actionTemplate.openModal('Doctor');
    actionDoctor.changeModalAction('register');
  };

  return (
    <LayananComponen
      handleAddDoctor={handleAddDoctor}
      handleCancelModal={handleCancelModal}
      colums={colums}
      data={listData}
      {...props}
    />
  );
}

const mapStateToProps = createStructuredSelector({
  listData: SelectorDoctor.listSelector(),
  modalDoctorIsShow: SelectorTemplate.modalDoctorSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionDoctor: bindActionCreators(DoctorAction, dispatch),
  actionTemplate: bindActionCreators(TemplateAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(LayananContainer);
