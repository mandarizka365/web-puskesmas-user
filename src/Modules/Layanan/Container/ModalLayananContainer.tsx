import * as DoctorAction from '../Store/LayananAction';
import * as SelectorDoctor from '../Selector/LayananSelector';

import { bindActionCreators, compose } from 'redux';

import ModalLayananComponen from '../Component/ModalLayananComponen';
import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

function ModalLayananContainer(props) {
  const { actionDoctor, doctorDetail, modalAction } = props;
  const handleOnSubmit = () => {
    if (modalAction === 'register') {
      actionDoctor.submitDoctorRequested();
    } else {
      actionDoctor.updateDoctorRequested();
    }
  };
  const initialValues: any = {};
  if (Object.keys(doctorDetail).length > 0) {
    const { name, poli, address, day, time, contact } = doctorDetail;
    initialValues.name = name;
    initialValues.poli = poli;
    initialValues.address = address;
    initialValues.day = day;
    initialValues.time = time;
    initialValues.contact = contact;
  }
  return (
    <ModalLayananComponen
      initialValues={initialValues}
      handleOnSubmit={handleOnSubmit}
      {...props}
    />
  );
}
const mapStateToProps = createStructuredSelector({
  doctorDetail: SelectorDoctor.selectedDoctorSelector(),
  modalAction: SelectorDoctor.modalActionSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionDoctor: bindActionCreators(DoctorAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(ModalLayananContainer);
