import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import AntrianContainer from '../Antrian/Container/AntrianContainer';
import DashboardContainer from '../Home/Container/HomeContainer';
import KegiatanContainer from '../Kegiatan/Container/KegiatanContainer';
import LayananContainer from '../Layanan/Container/LayananContainer';
import NomorContainer from '../Nomor/Container/NomorContainer';
import OldContainer from '../PasienLama/Container/OldContainer';
import PendaftaranContainer from '../Pendaftaran/Container/PendaftaranContainer';
import ProfilContainer from '../Profil/Container/ProfilContainer';
import WithTemplate from './WithTemplate';

export default class Navigation extends Component {
  render() {
    const home = WithTemplate(DashboardContainer);
    const pasienBaru = WithTemplate(PendaftaranContainer);
    const kegiatan = WithTemplate(KegiatanContainer);
    const profil = WithTemplate(ProfilContainer);
    const nomor = WithTemplate(NomorContainer);
    const antrian = WithTemplate(AntrianContainer);
    const layanan = WithTemplate(LayananContainer);
    const pasienLama =WithTemplate(OldContainer)
    return (
      <Switch>
        <Route path="/" exact={true} component={home} />
        <Route path="/pasienBaru" exact={true} component={pasienBaru} />
        <Route path="/pasienLama" exact={true} component={pasienLama} />
        <Route path="/kegiatan" exact={true} component={kegiatan} />
        <Route path="/profil" exact={true} component={profil} />
        <Route path="/layanan" exact={true} component={layanan} />
        <Route path="/nomor" exact={true} component={nomor} />
        <Route path="/antrian" exact={true} component={antrian} />
      </Switch>
    );
  }
}
