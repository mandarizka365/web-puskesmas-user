import { all, fork } from 'redux-saga/effects';
import { deleteDoctorAction, fetchDoctorListAction, submitDoctorAction, updateDoctorAction } from '../Layanan/Saga/LayananSaga';
import {
  deleteOldAction,
  fetchOldListAction,
  submitOldAction,
  updateOldAction,
} 
  from '../PasienLama/Saga/OldSaga'
import {
  deletePatientAction,
  fetchPatientListAction,
  submitPatientAction,
  updatePatientAction,
} from '../Pendaftaran/Saga/PendaftaranSaga';

export default function* () {
  yield all([
    fork(submitPatientAction),
    fork(updatePatientAction),
    fork(deletePatientAction),
    fork(fetchPatientListAction),
    fork(submitDoctorAction),
    fork(updateDoctorAction),
    fork(deleteDoctorAction),
    fork(fetchDoctorListAction),
    fork(submitOldAction),
    fork(deleteOldAction),
    fork(updateOldAction),
    fork(fetchOldListAction),
  ]);
}
