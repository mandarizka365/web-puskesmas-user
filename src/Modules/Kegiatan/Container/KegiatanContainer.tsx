import { Badge, Calendar, Col, Layout, Row } from 'antd';

import React from 'react';

const { Content } = Layout;

export default function KegiatanContainer() {
  function getListData(value) {
    let listData;
    switch (value.day()) { 
      case 0:
        listData = [
          { type: '', content: 'We are closed' },
        ];
        break;
      case 5:
        listData = [
          { type: '', content: 'Immunization Day!!' },
        ];
        break;
      case 6:
        listData = [
          { type: '', content: 'We are closed' },
        ];
        break;
      default:
    }
    return listData || [];
  }

  function dateCellRender(value) {
    const listData = getListData(value);
    return (
      <ul className="events">
        {listData.map(item => (
          <li key={item.content}>
            <Badge status={item.type} text={item.content} />
          </li>
        ))}
      </ul>
    );
  }

  function getMonthData(value) {
    if (value.month() === 8) {
      return 1394;
    }
    return 1394;
  }

  function monthCellRender(value) {
    const num = getMonthData(value);
    return num ? (
      <div className="notes-month">
        <section>{num}</section>
        <span>Backlog number</span>
      </div>
    ) : null;
  }
  return (
    
    <Layout>
      <Content className="content3">
      <Row>
        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
        <Calendar dateCellRender={dateCellRender} monthCellRender={monthCellRender} />
        </Col>
      </Row>
      </Content>
    </Layout>
  )
}
