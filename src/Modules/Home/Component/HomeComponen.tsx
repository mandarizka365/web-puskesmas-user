import { Button, Card, Carousel, Col, Layout, Row } from 'antd';

import React from 'react'
import images from '../../App/images'

const { Content } = Layout;
export default function HomeComponen() {

    return (
        <>
        <Layout>
          <Content className="content1">
          <Row>
            <Col span={24}>
              <Carousel autoplay >
                <div>
                <img src={images.gambarsatu} alt="doctor"  className="pict1"/>
                  </div>
                  <div>
                  <img src={images.gambardua}  alt="doctor"  className="pict1"/>
                  </div>
                  <div>
                  <img src={images.gambartiga}  alt="doctor"  className="pict1"/>
                  </div>
                  <div>
                  <img src={images.gambarempat}  alt="doctor"  className="pict1"/>
                  </div>
              </Carousel>
            </Col>
          </Row>
          <section className="homesection">
            <div>
                <Row className="rowhome">
                <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                  <Card className="cardkotak"  bordered={false}>
                    <img src={images.call}  alt="doctor"  className="pict3" href='/layanan'/><br/><br/>
                    <h3 className="h3">LAYANAN INFORMASI</h3><br/>
                    <p className="pCard"><b>02155677812</b></p>
                  </Card>
                </Col>
                <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                  <Card className="cardkotakdua" bordered={false}>
                    <img src={images.calendar}  alt="doctor"  className="pict3" href='/layanan'/><br/><br/>
                    <h3 className="h3">JADWAL KEGIATAN KAMI</h3><br/>
                      <Button className="button-look" href='/kegiatan'><b>LIHAT</b></Button>
                  </Card>
                </Col>
                <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                  <Card className="cardkotaktiga"  bordered={false}>
                  <img src={images.clock}  alt="doctor"  className="pict3"/><br/><br/>
                  <h3 className="h3">JAM PENDAFTARAN</h3>
                    <p className="pFirst">Senin - kamis 07.30 s/d 11.30 WIB</p>
                    <p className="pFirst">Jumat 07.30 s/d 10.30 WIB</p>
                    <p className="pFirst"> Sabtu 07.30 s/d 10.30 WIB</p>
                    <p className="pFirst">Minggu Libur</p>
                  </Card>
                </Col>
              </Row>
            </div>
          </section>
        </Content>
        <Content style={{width:'100%'}}>
          <Row>
            <Col span={24}>
              <section className="all articel one">
              <div>
                <a className="profil"  href="/profil">
                  <b>Lihat Profil Kami<i></i></b>
                </a>
            </div>
              </section>
            </Col>
          </Row>
        </Content>
        <Content style={{width:'100%'}}>
          <Row>
            <Col span={24}>
              <section className="layanan">
                <div className="divlayanan">
                  <div className="divlayanandua">
                    <h3 className="h3layanan">
                      <span>Layanan Kami</span>
                    </h3>
                  </div>
                  <div className="paragraph">
                    <p>
                    Masyarakat Kecamatan Kalikajar yang Sehat diartikan sebagai masyarakat yang<br/>
                    memiliki keadaan sehat, baik secara fisik, mental, spiritual maupun sosial yang <br/>
                    memungkinkan setiap orang untuk hidup produktif secara sosial<br/> dan ekonomis.
                    </p>
                  </div>
                </div>
              </section>
            </Col>
          </Row>
        </Content>
        <Content className="rowCol">
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        <Col span={6}>
            <Card className="card4" bordered={true}>
              <a href='/layanan'>
              <div className="pict4">
              <img src={images.gizi}  alt="doctor"    height="70"/>
              <p>Konsultasi Gizi</p>
              </div>
                </a>
            </Card>
          </Col>
          <Col span={6}>
            <Card className="card4" bordered={true}>
              <a href='/layanan'>
              <div className="pict4">
              <img src={images.umum}  alt="doctor"    height="70"/>
              <p>Poli Umum</p>
              </div>
                </a>
            </Card>
          </Col>
          <Col span={6}>
            <Card className="card4" bordered={true}>
              <a href='/layanan'>
              <div className="pict4">
              <img src={images.anak}  alt="doctor"    height="70"/>
              <p>Poli Anak</p>
              </div>
                </a>
            </Card>
          </Col>
          <Col span={6}>
            <Card className="card4" bordered={true}>
              <a href='/layanan'>
              <div className="pict4">
              <img src={images.gigi}  alt="doctor" height="70"/>
              <p>Poli Gigi</p>
              </div>
                </a>
            </Card>
          </Col>
        </Row>
        </Content>
        <Content className="rowColSnd">
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          <Col span={6}>
            <Card className="card4" bordered={true}>
              <a href='/layanan'>
              <div className="pict4">
              <img src={images.imunisasi}  alt="doctor"    height="70"/>
              <p>Poli Imunisasi</p>
              </div>
                </a>
            </Card>
          </Col>
          <Col span={6}>
            <Card className="card4" bordered={true}>
              <a href='/layanan'>
              <div className="pict4">
              <img src={images.tht}  alt="doctor"    height="70"/>
              <p>Poli THT</p>
              </div>
                </a>
            </Card>
          </Col>
          <Col span={6}>
            <Card className="card4" bordered={true}>
              <a href='/layanan'>
              <div className="pict4">
              <img src={images.kb}  alt="doctor"    height="70"/>
              <p>Poli KB</p>
              </div>
                </a>
            </Card>
          </Col>
          <Col span={6}>
            <Card className="card4" bordered={true}>
              <a href='/layanan'>
              <div className="pict4">
              <img src={images.kia}  alt="doctor"    height="70"/>
              <p>Poli KIA</p>
              </div>
                </a>
            </Card>
          </Col>
          </Row>
        </Content>
    </Layout>
        </>
    )
}
