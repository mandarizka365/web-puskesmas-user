import * as ActionPatient from '../Store/PendaftaranAction';
import * as ActionTemplate from '../../Template/Store/TemplateAction';
import * as SelectorPatient from '../Selector/PendaftaranSelector';

import { call, put, select, takeLatest } from 'redux-saga/effects';

import axios from 'axios';

function* fetchPatientListProcess() {
  try {
    const { data } = yield call(
      axios.get,
      `${process.env.REACT_APP_PATIENT_URL}/Patient/inquiry/0/1000`
    );
    yield put(ActionPatient.fetchPatientListFinished(data.data));
    console.log(data);
  } catch (error) {
    console.log(error);
  }
}

function* deletePatientProcces() {
  try {
    const id = yield select(SelectorPatient.idSelector());
    yield call(
      axios.delete,
      `${process.env.REACT_APP_PATIENT_URL}/Patient/${id}`
    );
    yield put(ActionPatient.fetchPatientListRequested());
  } catch (error) {
    console.log(error);
  }
}

function* submitPatientProcces() {
  try {
    const name = yield select(SelectorPatient.formNameSelector());
    const address = yield select(SelectorPatient.formAddressSelector());
    const dateOfBirth = yield select(
      SelectorPatient.formDateOfBirthSelector()
    );
    const gender = yield select(SelectorPatient.formGenderSelector());
    const nameParent = yield select(SelectorPatient.formNameParentSelector());
    const telephone = yield select(SelectorPatient.formTelephoneSelector());
    const nik = yield select(SelectorPatient.formNIKSelector());
    const noBPJS = yield select(SelectorPatient.formNoBPJSSelector());
    const registration = yield select(SelectorPatient.formRegistrationSelector());
    const poli = yield select(SelectorPatient.formPoliSelector());

    yield call(
      axios.post,
      `${process.env.REACT_APP_PATIENT_URL}/Patient/AddPatient`,
      {
        name,
        address,
        dateOfBirth,
        gender,
        nameParent,
        telephone,
        nik,
        noBPJS,
        registration,
        poli,
      }
    );

    yield put(ActionPatient.fetchPatientListRequested());
    yield put(ActionTemplate.openModal('Patient'));
  } catch (error) {
    console.log(error);
  }
}

function* updatePatientProcces() {
  try {
    const id = yield select(SelectorPatient.idSelector());
    const name = yield select(SelectorPatient.formNameSelector());
    const address = yield select(SelectorPatient.formAddressSelector());
    const dateOfBirth = yield select(
      SelectorPatient.formDateOfBirthSelector()
    );
    const gender = yield select(SelectorPatient.formGenderSelector());
    const nameParent = yield select(SelectorPatient.formNameParentSelector());
    const telephone = yield select(SelectorPatient.formTelephoneSelector());
    const nik = yield select(SelectorPatient.formNIKSelector());
    const noBPJS = yield select(SelectorPatient.formNoBPJSSelector());
    const registration = yield select(SelectorPatient.formRegistrationSelector());
    const poli = yield select(SelectorPatient.formPoliSelector());

    yield call(
      axios.put,
      `${process.env.REACT_APP_PATIENT_URL}/Patient/UpdatePatient`,
      {
        id,
        name,
        address,
        dateOfBirth,
        gender,
        nameParent,
        telephone,
        nik,
        noBPJS,
        registration,
        poli,
      }
    );

    yield put(ActionPatient.fetchPatientListRequested());
    yield put(ActionTemplate.openModal('Patient'));
  } catch (error) {
    console.log(error);
  }
}

export function* fetchPatientListAction() {
  yield takeLatest('FETCH_PATIENT_LIST_REQUESTED', fetchPatientListProcess);
}
export function* deletePatientAction() {
  yield takeLatest('DELETE_PATIENT_REQUESTED', deletePatientProcces);
}

export function* submitPatientAction() {
  yield takeLatest('SUBMIT_PATIENT_REQUESTED', submitPatientProcces);
}

export function* updatePatientAction() {
  yield takeLatest('UPDATE_PATIENT_REQUESTED', updatePatientProcces);
}