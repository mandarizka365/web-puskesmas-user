import { Col, Layout, Row } from 'antd';

import ModalPendaftaranContainer from '../Container/ModalPendaftaranContainer';
import React from 'react';

// import images from '../../App/images';

const { Content } = Layout;

export default function PendaftaranComponen(props) {

  return (
    <Layout>
      <Content className="content1">
      <div>
      <Row>
      <Col xs={24} sm={24} md={24} lg={12} xl={12}>
      <div className="pendaftaran">
        <p>Pendaftaran pasien baru diperuntukkan bagi pasien<br/> yang belum memiliki kartu berobat.</p>
        <p>Langkah pendaftaran online, isi form yang tersedia<br/> sesuai dengan data yang Anda miliki.</p>
        <p>Setelah pendaftaran selesai, pastikan muncul<br/> notifikasi "Pendaftaran Online Berhasil!!!"</p>
        <p>Klik Button Nomor Antrian untuk dapatkan nomor antrian<br/></p>
      </div>
      </Col>
      <Col xs={24} sm={24} md={24} lg={12} xl={12}>
      <ModalPendaftaranContainer {...props} />
      </Col>
      
    </Row>
      </div>
      </Content>
    </Layout>
  );
}
