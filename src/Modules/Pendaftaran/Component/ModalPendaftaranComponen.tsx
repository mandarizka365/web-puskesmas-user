import { Button, Form, notification } from 'antd';
import { DatePickers, InputText } from '../../../Assets/Components/CInputText';
import { Field, reduxForm } from 'redux-form';
import { Gender, Poli } from '../../../Assets/Components/Enum';

import { Link } from 'react-router-dom';
import React from 'react';
import { SelectGender } from '../../../Assets/Components/CSelectGender';
import { SelectPoli } from '../../../Assets/Components/CSelectPoli';
import validate from '../Validation/PendaftaranValidation';

function ModalPendaftaranComponen(props) {
  const openNotificationWithIcon = type => {
    notification[type]({
      message: 'Pendaftaran Online Berhasil!!',
      description:
        'Terima Kasih telah mengisi formulir pendaftaran sesuai ketentuan yang tertera.',
    });
  };

  const {
    handleOnSubmit,
    invalid,
  } = props;

  return (
    <div className="divform">
      <Form onFinish={handleOnSubmit} layout="horizontal">
        <Field
          name="name"
          component={InputText}
          label="Name"
          placeholder="Input name here"
        />

        <Field
          name="address"
          component={InputText}
          label="Address"
          placeholder="Input address here"
        />

        <Field
          name="dateOfBirth"
          component={DatePickers}
          label="Date Of Birth"
        />

        <Field
          name="gender"
          component={SelectGender}
          label="Gender"
          data={Gender}
        />

        <Field
          name="nameParent"
          component={InputText}
          label="Name Parent"
          placeholder="Input nameParent here"
        />

        <Field
          name="telephone"
          component={InputText}
          label="Telephone"
          placeholder="Input telephone here"
        />

        <Field
          name="nik"
          component={InputText}
          label="NIK"
          placeholder="Input nik here"
        />

        <Field
          name="noBPJS"
          component={InputText}
          label="No BPJS"
          placeholder="Input noBPJS here"
        />

        <Field
          name="registration"
          title="Tanggal Pendaftaran"
          component={DatePickers}
          placeholder="Input registration here"
        />

        <Field
          name="poli"
          component={SelectPoli}
          label="Tujuan Poli"
          data={Poli}
        />

        
          <Button
            type="primary"
            htmlType="submit"
            disabled={invalid}
            onClick={() => openNotificationWithIcon('success')}
            
          >
            Submit
          </Button>
          <Button
            type="primary"
            htmlType="submit"
            disabled={invalid}
            className="buttonnomor"
          >
            <Link to="/nomor"> Nomor Antrian </Link>
          </Button>
      </Form>
    </div>
      
  );
}

export default reduxForm({
  form: 'patientForm',
  validate,
  enableReinitialize: true,
})(ModalPendaftaranComponen); //HOC
