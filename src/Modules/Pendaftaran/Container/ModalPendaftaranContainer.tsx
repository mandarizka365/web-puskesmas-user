import * as PatientAction from '../Store/PendaftaranAction';
import * as SelectorPatient from '../Selector/PendaftaranSelector';

import { bindActionCreators, compose } from 'redux';

import ModalPendaftaranComponen from '../Component/ModalPendaftaranComponen';
import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

function ModalPendaftaranContainer(props) {
  const { actionPatient, patientDetail, modalAction } = props;
  const handleOnSubmit = () => {
    if (modalAction === 'register') {
      actionPatient.submitPatientRequested();
    } else {
      actionPatient.updatePatientRequested();
    }
  };
  const initialValues: any = {};
  if (Object.keys(patientDetail).length > 0) {
    const {
      name,
      address,
      dateOfBirth,
      gender,
      nameParent,
      telephone,
      nik,
      noBPJS,
      registration,
      poli,
    } = patientDetail;
    initialValues.name = name;
    initialValues.address = address;
    initialValues.dateOfBirth = dateOfBirth;
    initialValues.gender = gender;
    initialValues.nameParent = nameParent;
    initialValues.telephone = telephone;
    initialValues.nik = nik;
    initialValues.noBPJS = noBPJS;
    initialValues.registration = registration;
    initialValues.poli = poli;
  }
  return (
    <ModalPendaftaranComponen
      initialValues={initialValues}
      handleOnSubmit={handleOnSubmit}
      {...props}
    />
  );
}
const mapStateToProps = createStructuredSelector({
  patientDetail: SelectorPatient.selectedPatientSelector(),
  modalAction: SelectorPatient.modalActionSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionPatient: bindActionCreators(PatientAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(ModalPendaftaranContainer);
