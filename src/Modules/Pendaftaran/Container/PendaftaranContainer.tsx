import * as PatientAction from '../Store/PendaftaranAction';
import * as SelectorPatient from '../Selector/PendaftaranSelector';
import * as SelectorTemplate from '../../Template/Selector/TemplateSelector';
import * as TemplateAction from '../../Template/Store/TemplateAction';

import { Button, Layout } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons/lib/icons';
import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';

import PendaftaranComponen from '../Component/PendaftaranComponen';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

const { Content } = Layout;

function PatientContainer(props) {
  const { actionPatient, listData, actionTemplate } = props;

  const renderAction = row => {
    const handleUpdate = () => {
      actionPatient.changeModalAction('update');
      actionPatient.setPatientId(row.row.original.id);      
      actionPatient.setPatientDetail(row.row.original);
      actionTemplate.openModal('Patient');
    };
    const handleDelete = () => {
      actionPatient.setPatientId(row.row.original.id);
      actionPatient.deletePatientRequested(row.row.original.id);
    };
    return (
      <Layout>
        <Content className="content3">
          <React.Fragment>
         <Button
         icon={<EditOutlined />}
          className="btnActionTable"
          type="primary"
          onClick={handleUpdate}
        />
        
        <Button
        icon={<DeleteOutlined />}
          className="btnActionTable"
          type="primary"
          danger
          onClick={handleDelete}
        />

      </React.Fragment>
        </Content>
      </Layout>
      
    );
  };
  const colums = [
    {
      accessor: 'name',
    },
    {
      accessor: 'registration',
    },
    {
      accessor: 'address',
    },
    {
      accessor: 'gender',
    },
    {
      accessor: 'nik',
    },
    {
      accessor: 'noBPJS',
    },
    {
      Cell: renderAction,
    },
  ];

  useEffect(() => {
    actionPatient.fetchPatientListRequested();
  }, [actionPatient]);

  const handleAddPatient = () => {
    actionTemplate.openModal('Patient');
    actionPatient.changeModalAction('register');
  };

  const handleCancelModal = () => {
    actionTemplate.openModal('Patient');
    actionPatient.changeModalAction('register');
  };

  return (
    <PendaftaranComponen
      handleAddPatient={handleAddPatient}
      handleCancelModal={handleCancelModal}
      colums={colums}
      data={listData}
      {...props}
    />
  );
}

const mapStateToProps = createStructuredSelector({
  listData: SelectorPatient.listSelector(),
  modalPatientIsShow: SelectorTemplate.modalPatientSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionPatient: bindActionCreators(PatientAction, dispatch),
  actionTemplate: bindActionCreators(TemplateAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(PatientContainer);