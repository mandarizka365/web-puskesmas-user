export default function PendaftaranValidation(values) {
  const errors: any = {};
  if (!values.name) {
    errors.name = 'Name Required!';
  } else if (values.name.length < 3) {
    errors.name = 'Minimal Name 3 characters';
  }
  if (!values.address) {
    errors.address = 'Address Required!';
  }
  if (!values.dateOfBirth) {
    errors.dateOfBirth = 'Date Of Birth Required!';
  }if (!values.gender) {
    errors.gender = 'Gender Required!';
  }
  if (!values.nameParent) {
    errors.nameParent = 'Name Parent Required!';
  }
  if (!values.telephone) {
    errors.telephone = 'Telephone Required!';
  }
  if (!values.nik) {
    errors.nik = 'NIK Required!';
  }else if (values.nik.length < 16) {
    errors.nik = 'Minimal NIK 16 characters';
  }
  if (!values.registration) {
    errors.registration = 'Registration Date Required!';
  }
  if (!values.poli) {
    errors.poli = 'POLI Required!';
  }
  return errors;
}
