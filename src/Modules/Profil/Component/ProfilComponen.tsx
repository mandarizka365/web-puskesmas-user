import { Col, Layout, Row } from 'antd';

import React from 'react'
import images from '../../App/images'

const { Content } = Layout;

export default function DashboardContainer() {
    return (
        <>
        <Layout>
          <Content className="content4">
          <div className="site-card-wrapper">
              <Row >
                <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                <div className="visimisi">
                <p><b>VISI PUSKESMAS KAMI</b></p>
                <p>"MENJADIKAN"</p>
                </div>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                  <img src={images.gambarsatu}  alt="doctor"  className="pict1"/>
                </Col>
            </Row>
            <Row>
              <Col xs={24} sm={24} md={24} lg={12} xl={12}>
              <img src={images.gambardua}  alt="doctor"  className="pict1"/>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={12}>
              <div className="visimisi">
                <p><b>MISI PUSKESMAS KAMI</b></p>
                <p>1. Meningkatkan sumber daya kesehatan yang kompeten.</p>
                <p>2. Meningkatkan peran serta masyarakat umtuk berprilaku hidup bersih dan sehat melalui pendekatan keluarga</p>
              </div>
              </Col>
            </Row>
            <Row style={{marginBottom:20}}>
              <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                <div className="visimisi">
                  <p><b>MOTTO PUSKESMAS KAMI</b></p>
                  <p>” ANDA SEHAT KAMI SENANG ”</p>
                </div>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={12}>
              <img src={images.gambarlima}  alt="doctor"  className="pict1"/>
              </Col>
            </Row>
            </div>
          </Content>
        </Layout>
        </>
    )
}
