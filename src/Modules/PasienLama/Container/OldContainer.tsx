import * as OldAction from '../Store/OldAction';
import * as SelectorOld from '../Selector/OldSelector';
import * as SelectorTemplate from '../../Template/Selector/TemplateSelector';
import * as TemplateAction from '../../Template/Store/TemplateAction';

import { Button, Layout } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons/lib/icons';
import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';

import OldComponen from '../Component/OldComponen';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

const { Content } = Layout;


function OldContainer(props) {
  const { actionOld, listData, actionTemplate } = props;

  const renderAction = row => {
    const handleUpdate = () => {
      actionOld.changeModalAction('update');
      actionOld.setOldId(row.row.original.id);      
      actionOld.setOldDetail(row.row.original);
      actionTemplate.openModal('Old');
    };
    const handleDelete = () => {
      actionOld.setOldId(row.row.original.id);
      actionOld.deleteOldRequested(row.row.original.id);
    };
    return (
      <Layout>
        <Content className="content3">
        <React.Fragment>
         <Button
         icon={<EditOutlined />}
          className="btnActionTable"
          type="primary"
          onClick={handleUpdate}
        />
        
        <Button
        icon={<DeleteOutlined />}
          className="btnActionTable"
          type="primary"
          danger
          onClick={handleDelete}
        />

      </React.Fragment>
        </Content>
      </Layout>
    );
  };
  const colums = [
    {
      accessor: 'nik',
    },
    {
      accessor: 'date',
    },
    {
        accessor: 'poli',
    },
    {
        accessor: 'jenisBayar',
      },  
    {
      Cell: renderAction,
    },
  ];

  useEffect(() => {
    actionOld.fetchOldListRequested();
  }, [actionOld]);

  const handleAddOld = () => {
    actionTemplate.openModal('Old');
    actionOld.changeModalAction('register');
  };

  const handleCancelModal = () => {
    actionTemplate.openModal('Old');
    actionOld.changeModalAction('register');
  };

  return (
    <OldComponen
      handleAddOld={handleAddOld}
      handleCancelModal={handleCancelModal}
      colums={colums}
      data={listData}
      {...props}
    />
  );
}

const mapStateToProps = createStructuredSelector({
  listData: SelectorOld.listSelector(),
  modalOldIsShow: SelectorTemplate.modalOldSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionOld: bindActionCreators(OldAction, dispatch),
  actionTemplate: bindActionCreators(TemplateAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(OldContainer);
