import { Button, Form, notification } from 'antd';
import { Buy, Poli } from '../../../Assets/Components/Enum';
import { DatePickers, InputText } from '../../../Assets/Components/CInputText';
import { Field, reduxForm } from 'redux-form';

import { Link } from 'react-router-dom';
import React from 'react';
import { SelectBuy } from '../../../Assets/Components/CSelectBuy';
import { SelectPoli } from '../../../Assets/Components/CSelectPoli';
import validate from '../Validation/OldValidation';

function ModalOldComponen(props) {
  const openNotificationWithIcon = type => {
    notification[type]({
      message: 'Pendaftaran Online Berhasil!!',
      description:
        'Terima Kasih telah mengisi formulir pendaftaran sesuai ketentuan yang tertera.',
    });
  };
  
  const {
    handleOnSubmit,
    invalid,
  } = props;
 
  return (
    <div className="divform">
      <Form onFinish={handleOnSubmit} layout="vertical">
      <Field
          name="nik"
          component={InputText}
          label="NIK / No Kartu"
          placeholder="Input NIK / No Kartu here"
        />

        <Field
          name="registration"
          component={DatePickers}
          label="Date Registration"
        />
        
        <Field
          name="poli"
          component={SelectPoli}
          label="Tujuan Poli"
          data={Poli}/>

        <Field
          name="jenisBayar"
          component={SelectBuy}
          label="Jenis Bayar"
          data={Buy}
        />

        <Form.Item label=" " colon={false}>
          <Button type="primary" htmlType="submit" disabled={invalid} onClick={() => openNotificationWithIcon('success')}>
            Submit
          </Button>
          <Button
            type="primary"
            htmlType="submit"
            disabled={invalid}
            className="buttonnomor"
          >
            <Link to="/antrian"> Nomor Antrian </Link>
          </Button>
        </Form.Item>
      </Form>
    </div>
      
  );
}

export default reduxForm({
  form: 'oldForm',
  validate,
  enableReinitialize: true,
})(ModalOldComponen); //HOC
