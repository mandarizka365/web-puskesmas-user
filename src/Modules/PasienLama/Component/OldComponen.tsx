import { Col, Layout, Row } from 'antd';

import ModalOldContainer from '../Container/ModalOldContainer';
import React from 'react';

// import images from '../../App/images';

const { Content } = Layout;

export default function OldComponen(props) {

  return (
    <Layout>
      <Content className="content1">
      <Row>
      <Col xs={24} sm={24} md={24} lg={12} xl={12}>
      <div className="pendaftaran">
        <p>Pendaftaran pasien lama diperuntukkan bagi pasien<br/> yang telah memiliki kartu berobat.</p>
        <p>Isi formulir sesuai data yang Anda dimiliki.</p>
        <p>Setelah klik submit, pastikan muncul notifikasi<br/> "Pendaftaran Online Berhasil!!!"</p>
        <p>Klik Button Nomor Antrian untuk dapatkan nomor antrian<br/></p>
      </div>
      </Col>
      <Col xs={24} sm={24} md={24} lg={12} xl={12}>
      <ModalOldContainer {...props} />
      </Col>
    </Row>
      </Content>
    </Layout>
  );
}