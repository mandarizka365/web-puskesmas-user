export default function OldValidation(values) {
  const errors: any = {};
  if (!values.nik) {
    errors.nik = 'NIK / Card Number Required!';
  } else if (values.nik.length < 16) {
    errors.nik = 'Minimal NIK / Card Number 16 characters';
  }
  if (!values.registration) {
    errors.registration = 'Date Registration Required!';
  }
  if (!values.poli) {
    errors.poli = 'Poli Required!';
  }
  if (!values.jenisBayar) {
    errors.jenisBayar = 'Type of Payment Required!';
  }
  return errors;
}
